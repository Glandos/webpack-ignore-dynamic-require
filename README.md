# Webpack / Ignore dynamic `require`

When you are using webpack for a backend project, some projects try to use `require` calls with variable, e.g. to load configuration files. Webpack is usually confused by this, so this plugin will simply ignore them, and let the call as-is.

## Usage

The plugin takes no options. Simply require it and invoke like any other plugin.

```js
// webpack.config.js
const IgnoreDynamicRequire = require('webpack-ignore-dynamic-require');

module.exports = {
  // ...
  plugins: [
    new IgnoreDynamicRequire()
  ]
}
```
